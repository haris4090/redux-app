export interface ITodo{
    id: Number;
    description: string;
    responsible: string;
    priority: string;
    isCompleted: boolean;
}